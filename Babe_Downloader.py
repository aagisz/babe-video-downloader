try:
    from bs4 import BeautifulSoup
    import wget, json, requests, os
    from http import HTTPStatus
except Exception as e:
    print(e)
    exit()

if __name__ == "__main__":
    print('-Babe Video Downloader-')
    link = input('Babe link : ')
    try:
        r = requests.get(link)
        if r.status_code == 200:
            print('Status return : {} {}'.format(r.status_code, HTTPStatus(r.status_code).phrase))
            soup = BeautifulSoup(r.content, 'html5lib').find_all('script')
            soup = str(soup[3].string).split(';')
            soup = soup[0] + soup[1]
            data = json.loads(soup[36:])
            title = data.get('title')
            article_id = data.get('articleId')
            video_id = data.get('video', {}).get('videoId')
            video_type = data.get('video', {}).get('videoList', {}).get('video_1', {}).get('vtype')
            video_link = data.get('video', {}).get('videoList', {}).get('video_1', {}).get('main_url')
        else:
            print('Status return : {} {}'.format(r.status_code, HTTPStatus(r.status_code).phrase))
            exit()
        print('Title : {}\nArticle ID : {}\nVideo ID : {}\nVideo format : {}\nVideo link : {}'.format(title, article_id, video_id, video_type, video_link))
        if not os.path.exists('Downloads'):
            try:
                os.mkdir('Downloads')
                print('Directory create successfully!.')
            except Exception as e:
                print('Failed create directory!. {}'.format(e))
                exit()
        try:
            download = wget.download(video_link, out='Downloads\\'+video_id+'.'+video_type)
        except Exception as e:
            print('\nCant download this video\n{}'.format(e))
            exit()
    except Exception as e:
        print(e)
        exit()